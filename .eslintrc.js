module.exports = {
  parser: '@typescript-eslint/parser',
  env: {
    es2021: true,
    node: true
  },
  plugins: ['@typescript-eslint', 'prettier'],
  extends: [
    'plugin:@typescript-eslint/recommended',
    'prettier',
    'plugin:prettier/recommended',
    'standard'
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module'
  },
  rules: {
    'prettier/prettier': [
      'error',
      {
        singleQuote: true,
        semi: false,
        allowParens: 'avoid',
        trailingComma: 'none',
        endOfLine: 'auto'
      }
    ],
    'space-before-function-paren': ['error', 'never'],
    indent: 'off',
    '@typescript-eslint/indent': ['error', 2, { MemberExpression: 1 }],
    camelcase: 'off'
  }
}
