import { QueryInterface, DataTypes } from 'sequelize'

export async function up(q: QueryInterface): Promise<void> {
  await q.createTable('cards', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    content: {
      type: DataTypes.STRING,
      allowNull: false
    },
    column: {
      type: DataTypes.STRING,
      allowNull: false
    }
  })
}

export async function down(q: QueryInterface): Promise<void> {
  await q.dropTable('cards')
}
