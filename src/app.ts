import express from 'express'
import cors from 'cors'
import 'dotenv/config'

import { router } from './routes'

export class App {
  private express: express.Application
  private port = parseInt(process.env.DB_PORT) || 5000
  private host = process.env.DB_HOST

  public constructor() {
    this.express = express()
    this.middlewares()
    this.listen()
    this.routes()
  }

  private middlewares(): void {
    this.express.use(express.json())
    this.express.use(cors())
  }

  private listen(): void {
    this.express.listen(this.port, this.host, () =>
      console.log(`Servidor rodando em http://${this.host}:${this.port}`)
    )
  }

  private routes(): void {
    this.express.use(router)
  }

  public getApp(): express.Application {
    return this.express
  }
}
