import { Model, DataTypes, Optional, Sequelize, Options } from 'sequelize'

import config from '../config/config.json'

const dbConfig: Options = {
  dialect: 'sqlite',
  storage: config.development.storage
}

const sequelize = new Sequelize(dbConfig)

type Column = 'TODO' | 'DOING' | 'DONE'

interface CardAttributes {
  readonly id: number
  title: string
  content: string
  column: Column
  // created_at: Date
  // update_at: Date
}

// eslint-disable-next-line prettier/prettier
type CardCreationAttributes = Optional<CardAttributes, 'id' >

class Card
  extends Model<CardAttributes, CardCreationAttributes>
  implements CardAttributes {
  public id!: number
  public title!: string
  public content!: string
  public column!: Column

  // public readonly created_at!: Date
  // public readonly update_at!: Date
}

Card.init(
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    content: {
      type: DataTypes.STRING,
      allowNull: false
    },
    column: {
      type: DataTypes.STRING,
      allowNull: false
    }
  },
  {
    sequelize,
    tableName: 'cards'
  }
)

export default Card
