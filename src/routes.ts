import express from 'express'
import CardController from './controllers/CardController'

const router = express.Router()

router.post('/login/', (req, res) => {
  return res.json({ msg: 'logou porra!' })
})

router.get('/cards/', CardController.index)
router.post('/cards/', CardController.create)
router.put('/cards/:id', CardController.update)
router.delete('/cards/:id', CardController.delete)

export { router }
